const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('note', new Schema({
    userId: {
        required: true,
        type: String,
    },
    text: {
        required: true,
        type: String,
    },
    createdDate: {
        type: Date,
        default: new Date()
    },
    completed: {
        type: Boolean,
        default: false
    },
}))