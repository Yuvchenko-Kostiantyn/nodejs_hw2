const jwt = require('jsonwebtoken');
const SECRET = 'secret';

module.exports.authorize = (req, res, next) => {
    const authHeader = req.headers['authorization'];

    if(!authHeader){
        return res.status(401).json({status: 'Unauthorized user'})
    }


    try{
        req.user = jwt.verify(authHeader, SECRET);
        next();
    } catch (err) {
        next({status: 401, message: 'Unauthorized user'});
    }
}