module.exports = {
    NO_SUCH_NOTE: {status: 400, message: 'Invalid note id'},
    INVALID_ID: {status: 404, message:'Invalid note id value'},
    INVALID_PARAMETERS: {status: 400, message: 'Invalid parameters'},
    INVALID_KEYS: {status: 400, message:'No user with that set of keys found'}
}