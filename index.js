const express =  require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const { authorize } = require('./middleware/authMiddleware');
const { errorHandler } = require('./middleware/errorHandler');
const { DB_USER, DB_PASSWORD, DB_NAME } = require('./config/dbconfig')

const PORT = process.env.PORT || 8080;

mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@main-cluster.qnnvg.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
}).then(res => console.log('DB connected'));

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const notesRouter = require('./routers/notesRouter');

app.use(cors())
app.use(express.json());


app.use('/api/auth', authRouter);

app.use(authorize);
app.use('/api/users/me', userRouter);
app.use('/api/notes', notesRouter);
app.use(errorHandler);

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));