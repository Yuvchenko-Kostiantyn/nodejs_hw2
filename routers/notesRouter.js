const express = require('express');
const router = express.Router();

const { getNotes, addNote, getNoteById, updateNoteById, changeNoteStatus, deleteNote } = require('../controllers/notesController')

router.get('/', getNotes);
router.post('/', addNote);
router.get('/:id', getNoteById);
router.put('/:id', updateNoteById);
router.patch('/:id', changeNoteStatus);
router.delete('/:id', deleteNote);

module.exports = router