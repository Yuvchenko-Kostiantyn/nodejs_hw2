const Note = require('../models/note');
const errors = require('../config/errors')


module.exports.getNotes = (req, res) => {
    Note.find({userId: req.user._id}).exec()
    .then(data => {
        res.json({notes:data})
    }).catch(err => {
        res.status(500).json({message: err.message})
    })
};

module.exports.addNote = (req, res, next) => {
    const { text } = req.body;
    const userId = req.user._id;
    if(!text){
        throw errors.INVALID_PARAMETERS;
    }

    const note = new Note({userId, text});
    note.save()
    .then(() => {
        res.json({status: 'Success'})
    })
    .catch(err => {
        next({status: err.status || 500, message: err.message})
    })
}

module.exports.getNoteById = (req, res, next) => {
    Note.findById(req.params.id).exec()
    .then((data) => {
        if(!data){
            throw errors.NO_SUCH_NOTE;
        }
        res.json(data)
    }).catch(err => {
        if(err.kind === 'ObjectId'){
            next(errors.INVALID_ID);
        }
        next({status: err.status || 500, message: err.message})
    });
}

module.exports.updateNoteById = (req, res, next) => {
    const {text} = req.body

    if(!text){
        throw errors.INVALID_PARAMETERS;
    }

    Note.findByIdAndUpdate(req.params.id, {text: text}).exec()
    .then(note => {
        if(!note){
            throw errors.NO_SUCH_NOTE;
        }
        res.json({message: 'Success'})
    }).catch(err => {
        if(err.kind === 'ObjectId'){
           next(errors.INVALID_ID);
        }
        next({status: err.status || 500, message: err.message})
    })
}

module.exports.changeNoteStatus = async (req, res, next) => {
    try{
        const data = await Note.findById(req.params.id);
        if(!data){
           throw errors.NO_SUCH_NOTE;
        }
        data.completed = !data.completed
        await data.save();
        res.json({message: 'Success'})
    } catch (err){
        if(err.kind === 'ObjectId'){
            next(errors.INVALID_ID)
        }
        next({status: err.status || 500, message: err.message})
    }
}

module.exports.deleteNote = async (req, res, next) => {
    try{
        await Note.findByIdAndDelete(req.params.id);
        res.json({message: 'Success'})
    } catch (err) {
        if(err.kind === 'ObjectId'){
            return next(errors.INVALID_ID)
        }
        next({status: 500, message:'Internal server error'})
    }
}