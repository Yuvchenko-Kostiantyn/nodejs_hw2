const User = require('../models/user');
const jwt = require('jsonwebtoken');
const errors = require('../config/errors')


module.exports.register = (req, res, next) => {
    const {username, password} = req.body;
    const createdDate = new Date();

    const user = new User({username, password, createdDate})
    user.save()
    .then(()=>{
        res.json({status: 'Success'})
    })
    .catch(err => {
        if(!username || !password){
            return next(errors.INVALID_PARAMETERS)
        }
        next({status: err.status || 500, message: err.message})
    })
}

module.exports.login = (req, res, next) => {
    const {username, password} = req.body;
    if(!username || !password){
        throw errors.INVALID_PARAMETERS;
    }

    User.findOne({username, password}).exec()
    .then((user)=>{
        if(!user){
            throw errors.INVALID_KEYS;
        }
        res.json({status: 'ok', jwt_token: jwt.sign(JSON.stringify(user), process.env.SECRET)})
    })
    .catch(err => {
        next({status: err.status || 500, message: err.message});
    })
}