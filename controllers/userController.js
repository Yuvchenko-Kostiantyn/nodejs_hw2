const User = require('../models/user')

module.exports.getUser = (req, res, next) => {
    const {_id, username, createdDate} = req.user
    try{
        res.json({user: {_id, username, createdDate}});
    } catch(err){
        next({status: err.status || 500, message: err.message});
    }
}

module.exports.deleteUser = (req, res, next) => {
    User.findByIdAndDelete({_id: req.user._id}).exec()
    .then(() => {
        res.json({message: 'Success'});
    }).catch(err => {
        next({status: err.status || 500, message: err.message});
    })
}

module.exports.changePassword = (req, res, next) => {
    const { oldPassword, newPassword } = req.body;

    User.findByIdAndUpdate(req.user._id, {password: newPassword}).exec()
    .then(user => {
        if(user.password !== oldPassword){
            throw {status: 400, message:'Invalid Password'};
        }
            res.json({message: 'Success'});
    }).catch(err => {
        next({status: err.status || 500, message: err.message});
    });
}